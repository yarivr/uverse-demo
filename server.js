var gcm = require('node-gcm');
var express = require('express');
var app = express();
app.use(express.static(__dirname + '/public'));

//API Server Key
var sender = new gcm.Sender('AIzaSyBUT46igZKsgjWEtKZC2KPL7pNemTyYTgA');
var registrationIds = [];
var lines = [];

// At least one reg id required 
//registrationIds.push('APA91bHoCpgT9ynsOrIcnXCxaejPEOy6UNxZZZArLjN-Mq7C7ZyiJL8HnTak-Rlad8eVyf0tKtarINylDHBhBMeF9df3cQIEZaDn20exQGVRN2mDpa0iVmP4TU7ezVWvhgQZK-n2sCoQXjdLNuB4ZfLsl5vRYlIwkQ');


app.get('/', function (req, res) {
    res.send('hello world');
});
app.get('/push', function (req, res) {
    var message = new gcm.Message();
    // Value the payload data to send...
    message.addData('message', "Click to chat");
//    message.addData('message', "\u270C Peace, Love \u2764 and PhoneGap \u2706!");
    message.addData('title', 'U-Verse Notification');
    message.addData('msgcnt', '1'); // Shows up in the notification in the status bar
    message.addData('soundname', 'beep.wav'); //Sound to play upon notification receipt - put in the www folder in app
    //message.collapseKey = 'demo';
    //message.delayWhileIdle = true; //Default is false
    message.timeToLive = 3000;// Duration in seconds to hold in GCM and retry before timing out. Default 4 weeks (2,419,200 seconds) if not specified.
    sender.send(message, registrationIds, 4, function (result) {
        console.log(result);
    });
    res.send('push - OK');
});

app.get('/register/:id', function (req, res) {
    var id = req.params.id;
    console.log("RegisteredID:",id);
    if (registrationIds.indexOf(id) == -1) {
        registrationIds.push(id);
    }
    res.send('register - OK');
});

app.get('/addlines/:json', function (req, res) {
    var newLines = JSON.parse(req.params.json);
    console.log(newLines);
    lines = lines.concat(newLines);
    res.send('addlines' + JSON.stringify(lines));
});

app.get('/getlines', function (req, res) {
    res.json(lines);
    lines = [];
});

app.listen(3000);