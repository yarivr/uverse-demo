var app = angular.module("uverse", ["ngSanitize", "ngAnimate"]);
app.controller("MainCtrl", function ($scope, $timeout, $http) {
    var host = "localhost:3000";

    $scope.ui = {
        video: {
            index: -1,
            height: getVideoHeight(),
            visible: false,
            partial: false,
            list: [
                {
                    src: "images/cook.mp4",
                    channel: "1102 Food Network",
                    title: "Master Chef",
                    duration: "9:00 - 10:00 PM",
                    logo: "food"
                },
                {
                    src: "images/kids.mp4",
                    channel: "1103 Nickelodeon",
                    title: "Spongebob Squarepants",
                    duration: "9:20 - 10:10 PM",
                    logo: "nick"
                },
                {
                    src: "images/jimmy.mp4",
                    channel: "1104 ABC",
                    title: "Jimmy Kimmel Live",
                    duration: "9:15 - 10:05 PM",
                    logo: "abc"
                }
            ]
        },
        settings: true,
        settingsBtn: false,
        info: {
            open: false
        },
        demoStarted: false,
        configure: false,
        chatTimer: null,
        adTimer: null,
        add: {
            visible: false
        },
        chat: {
            visible: false,
            lines: []
        }
    };

    function getVideoHeight() {
        return window.innerHeight - 5;
    }

    window.addEventListener("resize", function () {
        $scope.$apply(function () {
            $scope.ui.video.height = getVideoHeight();
        });
    });

    function showAd() {
        $timeout(function () {
            $scope.ui.add.visible = true;
            sendNotification();
            $timeout(function () {
                $scope.ui.add.visible = false;
                $scope.ui.video.list[$scope.ui.video.index].src = "images/jimmy.mp4"
                var video = $("#video");
                video.bind("loadeddata", function () {
                    video.unbind("loadeddata");
                    $scope.$apply(function () {
                        video[0].currentTime = 10;
                        video[0].volume = 0.5;
                    });
                });

            }, 11000);
        }, 51000);
        $scope.ui.video.list[$scope.ui.video.index].src = "images/comercial.mp4"
        var video = $("#video");
        video.bind("loadeddata", function () {
            video.unbind("loadeddata");
            $scope.$apply(function () {
                video[0].currentTime = 0;
            });
        });
    }

    function sendNotification() {
        $http.get("/push");
    }

    function onNewLine(lineObj) {
        $scope.ui.chat.lines.push(lineObj);
        var chatContent = $("#chatContent");
        chatContent.animate({scrollTop: chatContent[0].scrollHeight}, "slow");
        $timeout(onNewLine, 1000);
    }

//    function onShowChat() {
//        $scope.ui.add.visible = false;
//        $scope.ui.chat.visible = true;
//        $timeout(onNewLine, 1000);
//    }

    function getLines() {
//        if ($scope.ui.video.index == 2) {
        $http.get("/getlines").success(function (lines) {
            try {
                if (lines.length > 0) {
                    var text = lines[0].text;
                    if (text.indexOf("__") == 0 && text != "__SWIPED") {
                        switch (text) {
                            case "__REMOVE":
                                $scope.ui.chat.visible = false;
                                break;
                            case "__CHOOSE":
                                showRemoteBG();
                                break;
                            case "__DONE":
                                configure();
                                break;
                            case "__PCI":
                                break;
                            case "__VIDEO":
                                showPartial();
                                break;
                        }

                    } else {
                        $scope.ui.chat.visible = true;
                        $scope.ui.video.partial = false;
                        $scope.ui.add.visible = false;
                        $scope.ui.chat.lines = $scope.ui.chat.lines.concat(lines);
                        var chatContent = $("#chatContent");
                        chatContent.animate({scrollTop: chatContent[0].scrollHeight}, "slow");
                    }
                }
            } catch (e) {

            }
        });
        $timeout(getLines, 1000);
//        }
    }

    function showPartial() {
        $scope.ui.video.partial = true;
        $timeout(function () {
            var partial = $("#partial")[0];
            partial.volume = 0;
            partial.play();
        }, 1000);
    }

    function showRemoteBG() {
        $(document.body).addClass("remote");
    }

    function configure() {
        $scope.ui.configure = true;
        $timeout(function () {
            $(document.body).removeClass("remote").addClass("configuration");
            $scope.ui.configure = false;
            $timeout(function () {
                $(document.body).removeClass("configuration")
            }, 3000);
        }, 3000);
    }

    $scope.getAvatar = function (type) {
        if (type === "agent") {
            return "images/agent.jpg";
        } else {
            return "images/visitor.jpg";
        }
    };

    $scope.settingsChat = function () {
        $scope.ui.settings = false;
        $scope.ui.settingsBtn = false;
        sendNotification();
    };


    $("#wrapper").click(function () {
        if (!$scope.ui.settings) {
            var video = $("#video");
            $scope.ui.chat.visible = false;
            $scope.$apply(function () {
                $scope.ui.demoStarted = true;
                if ($scope.ui.video.index < 2) {
                    $scope.ui.video.index++;
                } else {
                    $scope.ui.video.index = 0;
                }
                if ($scope.ui.video.index == 2) {
                    video.bind("loadeddata", function () {
                        video.unbind("loadeddata");
                        $scope.$apply(function () {
                            $scope.ui.adTimer = $timeout(showAd, 10000);
                            video[0].currentTime = 125;
                        });
                    });
                } else {
                    $timeout.cancel($scope.ui.adTimer);
                    $timeout.cancel($scope.ui.chatTimer);
                    $scope.ui.add.visible = false;
                }
                $scope.ui.video.visible = true;
                $scope.ui.info.open = true;
                video[0].play();
                $timeout(function () {
                    $scope.ui.info.open = false;
                }, 2000);
            });
        } else {
            $(document.body).addClass("settings");
            $scope.ui.settingsBtn = true;
        }
    });

    getLines();
});